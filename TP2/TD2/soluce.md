# Architecture et système : TD 2 #

## Exercice 1 ##

1) Comment initialiser un registre à 0 ?
	AND R0, R1, 0
2) * On doit d'abord initialiser le registre.
	AND R0,R0,0
*   Mémorisation d'une d'une constante sur 5 bits.
	ADD R0, R0
*   Mémorisation d'une constante sur 16 bits.
	(ici est une valeur)
	LD R0, ici
	ici .FILL
3) Code somme de deux valeur contenues aux adresse NUM1 et NUM2, son résultat est sauvegardé dans SUM.
```assembler
.ORIG x3000
LD R1, num1
LD R2, num2
AND R0, R0, 0
ADD R0, R1, R2
ST, R0, SUM
num1 .FILL 10
num2 .FILL 5
sum  .FILL 0
.END
```

4)  Comment peut-on vérifier si la valeur contenue dans un registre général est 0?
BRz exi : Le BRz est une condition qui teste si l'instruction est égale ou différent de 0.
	Dans ce cas, si BRz est vérifié, on fait appel a exi, sinon on passe a la ligne suivante du programme.

On peut aussi tester sur BRn et BRp qui test respectivement si la derniere valeur stocké est négatif ou positif.

exi est une fonction qui permet de quitter la fonction

```.assembler
AND R1, R1, 0
ADD R1, R1, 1
LD  R0, NUM
BRz exi
AND R1, R1, 0
num .FILL _
exi:
	.END
```

Le _ à coté du FILL c'est une constante.

5) Code LC3 qui affecte Ri = - Ri

```assembler
LD  R0, NUM
NOT R0, R0
ADD R0, R0, 1
(EXEMPLE)
NUM: .FILL _
.FILL permet d'affecter une constante (codé sur 16bit)  sur une étiquette (Variable)
```


6) Soustraction

```asm
LD  R1, num1
LD  R2, num2
NOT R2, R2
ADD R2, R2, 1
AND R0, R0, 0
ADD R0, R1, R2
(Exemple)
num1: .FILL 28
num2: .FILL 10
```
## Exercice 2 ##
1) C'est la même chose que l'exercice 1 question 4.

2)
```assembly
.ORIG x3000
LD R1, num1
LD, R2, num2
AND R3, R3, 0
NOT R3,R2
ADD R3, R3, 1
AND R0, R0, 0
ADD R0, R1, R3
BRp positif
AND R0,	R0,	0
ADD	R0,	R0,	R2
BR	exit
pos:	AND R0, R0, 0
			ADD R0, R0 R1
num1: .FILL
num2:	.FILL
exit: .END
```

## Exercice 3 ##

```assembler
val:
	.FILL	10
	.FILL	8
	.FILL	15
	.BKLW	1
```

```assembler
LEA	R0,val
```

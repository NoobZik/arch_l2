/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   exo2.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: NoobZik <rakib.hernandez@gmail.com>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/11/14 11:34:53 by NoobZik           #+#    #+#             */
/*   Updated: 2017/11/14 12:14:01 by NoobZik          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdio.h>
#include <fcntl.h>
#include <unistd.h>

int main(int argc, char const *argv[]) {

	if (argc < 2) {
		printf("Pas de commande\n");
		return 0;
	}

	int fd;

	if (!(fd = open(argv[2], O_RDONLY))) {
		perror("Erreur de lecture fichier");
	}
	close(fd);
	return 0;
}

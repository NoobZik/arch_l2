/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   exercice_1.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: NoobZik <rakib.hernandez@gmail.com>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/09/19 02:00:35 by NoobZik           #+#    #+#             */
/*   Updated: 2017/09/19 04:03:29 by NoobZik          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>
#include <stdio.h>
#include <string.h>

char  *ft_strrev(char *str);

/**
 * Affiche les représentation binaire de k, du plus faible au plus fort
 * @param  argc [description]
 * @param  argv [description]
 * @return      [EXIT SUCCESS]
 */
int main(int argc, char const *argv[]) {
  (void)  argc;
  (void)  argv;

  int     k;
  char    res[50] = "";

  printf("Un nombre entier k : ");

  if(!scanf("%d", &k))
    perror("scanf :");

  while(k != 0){
    if(k % 2 == 0)
      strcat(res, "0");
    else
      strcat(res, "1");
    k /= 2;
  }

  ft_strrev(res);
  printf("Conversion binaire : (%s)2\n", res);
  return 0;
}

/**
 * ft_strrev permet d'inverser une chaine de caractère
 * @param  str pointeur sur chaine
 * @return     une chaine inversé
 */
char  *ft_strrev(char *str){
  int  i;
  int  l = 0;
  char t;

  while (str[l] != '\0')
	 l++;

  i = -1;
  while (++i < --l){
   t = str[i];
   str[i] = str[l];
   str[l] = t;
  }

  return (str);
}

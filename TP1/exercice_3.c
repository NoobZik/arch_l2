/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   exercice_3.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: NoobZik <rakib.hernandez@gmail.com>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/09/24 16:17:12 by NoobZik           #+#    #+#             */
/*   Updated: 2017/09/24 17:19:27 by NoobZik          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

/*
  Note à moi même : Complétude à deux : Additions de nombre binaire avec/sans
                    le signe négatif

  To be finished : Addition
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

char  *ft_strrev(char *str);
void buffer_overflow(int k, unsigned int n);
char *foo_dec2bin(char *str, int k, unsigned int n);
void ft_printstr(char *str, int s, unsigned int n);
void reverse_bytes(char *str);
char *addition_bytes(char *str1, char *str2);

int main(int argc, char const *argv[]) {
  (void)            argc;
  (void)            argv;
  int               k;
  int               l;
  int               s;
  int               t;
  unsigned  int     n;
  unsigned  int     m;
  char              *res;
  char              *res2;
  char              *res3;

  printf("Saisissez K :");
  if(!scanf("%d", &k))
    perror("scanf :");
  printf("Saisissez n bits (2^k): ");
  if(!scanf("%u", &n))
    perror("scanf :");

  printf("Saisissez l :");
  if(!scanf("%d", &l))
    perror("scanf :");
  printf("Saisissez m bits (2^k): ");
  if(!scanf("%u", &m))
    perror("scanf :");

  s = k;
  t = l;

  res  = calloc(n, sizeof(int));
  res2 = calloc(m, sizeof(int));

  res  = foo_dec2bin(res, s, n);
  res2 = foo_dec2bin(res2, t, m);

  reverse_bytes(res);
  ft_printstr(res, s, n);
  reverse_bytes(res2);
  ft_printstr(res2, t, m);

  res3 = addition_bytes(res, res2);

  free(res);
  free(res2);
  free(res3);
  return 0;
}

/**
 * ft_strrev Reverse an array of char
 * @param  *str string to reverse
 * @return      a reversed string
 */
char  *ft_strrev(char *str){
  int  i;
  int  l = 0;
  char t;

  while (str[l] != '\0')
    l++;

  i = -1;
  while (++i < --l){
    t = str[i];
    str[i] = str[l];
    str[l] = t;
  }

  return (str);
}

/**
  * Print a formatted message in case of a char array buffer overflow with
  * @param k Decimal unsigned integer
  * @param n Memory allocation unsigned integer
  */
void buffer_overflow(int k, unsigned int n) {
  printf("Depassement de quota k = %u n = %u\n", k, n);
}

/**
 * Convert a decimal formatted integer into a binary string formatted
 * @param  str Previously initialised pointer to char
 * @param  k   Current decimal number
 * @param  n   Lenght of the bytes to be represented
 * @return     A string formatted binary number without useless 0.
 */
char *foo_dec2bin(char *str, int k, unsigned int n){
  while(k != 0){
    (k % 2 == 0) ?
    strcat(str, "0"):
    strcat(str, "1");
    k /= 2;
  }

  if(k == 0){
    ft_strrev(str);
  }
  else{
    buffer_overflow(k,n);
    return NULL;
  }

  return str;
}

/**
 * This function print the binary number
 * @param str binary number string formatted
 * @param s   original decimal number
 * @param n   size of the given array prior Conversion
 */
void ft_printstr(char *str, int s, unsigned int n){
  printf("Conversion binaire de %d sur %d bits: (%s)%u\n", s, n, str, n);
}

/**
 * This function flip 0 and 1
 * @param str a flipped bytes
 */
void reverse_bytes(char *str){
  int i = 0;
  while(str[i] != '\0'){
    if(str[i] == '0')
      str[i] = '1';
    else
      str[i] = '0';
    i++;
  }
}

/**
 * return the result of two add of binary number
 * @param  str1 binary 1
 * @param  str2 binary 2
 * @return      binary1 + binary2
 */
char *addition_bytes(char *str1, char *str2){
  char *res;
  if(sizeof(str1) < sizeof(str2)){
    res = calloc(sizeof(str2)+1, sizeof(int));
  }
  else
    res = calloc(sizeof(str1)+1, sizeof(int));
  return res;
}

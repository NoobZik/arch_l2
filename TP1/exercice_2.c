/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   exercice_2.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: NoobZik <rakib.hernandez@gmail.com>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/09/19 04:08:49 by NoobZik           #+#    #+#             */
/*   Updated: 2017/09/24 16:10:06 by NoobZik          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

/*
    Note :  Ce programme contient des ecriture vers mémoire ram non autorisé
            je ne sais pas comment je pourrais y remedier...
            Le problème vients surement au niveau du calloc, on a pas de
            condition qui permet de verifier un eventuel depassement de mémoire

            Sinon la conversion en base binaire marche niquel !
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

char  *ft_strrev(char *str);
void buffer_overflow(unsigned int k, unsigned int n);


int main(int argc, char const *argv[]) {
  (void)            argc;
  (void)            argv;
  unsigned  int     k;
  unsigned  int     s;
  unsigned  int     n;
//  char              res[50] = "0";
  char              *res2;

  printf("Saisissez K :");
  if(!scanf("%u", &k))
    perror("scanf :");
  printf("Saisissez n bits (2^k): ");
  if(!scanf("%u", &n))
    perror("scanf :");

  s = k;
  res2 = calloc(n,sizeof(int));

  while(k != 0){
    (k % 2 == 0) ?
    strcat(res2, "0"):
    strcat(res2, "1");
    k /= 2;
  }

  if(k == 0){
    ft_strrev(res2);
    printf("Conversion binaire de %u sur %d bits: (%s)%u\n", s, n, res2, n);
  }
  else
    buffer_overflow(k,n);

  free(res2);
  return 0;
}

/**
* ft_strrev Reverse an array of char
* @param  *str string to reverse
* @return      a reversed string
*/
char  *ft_strrev(char *str){
 int  i;
 int  l = 0;
 char t;

 while (str[l] != '\0')
  l++;

 i = -1;
 while (++i < --l){
  t = str[i];
  str[i] = str[l];
  str[l] = t;
 }

 return (str);
}

/**
 * Print a formatted message in case of a char array buffer overflow with
 * @param k Decimal unsigned integer
 * @param n Memory allocation unsigned integer
 */
void buffer_overflow(unsigned int k, unsigned int n) {
  printf("Depassement de quota k = %u n = %u\n", k, n);
}

/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ex01.h                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: NoobZik <rakib.hernandez@gmail.com>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/12/05 10:51:56 by NoobZik           #+#    #+#             */
/*   Updated: 2017/12/05 15:44:02 by NoobZik          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef _EX01_H_
#define _EX01_H_
#include <stdbool.h>

typedef struct rect_s {
  char        name[20];
  char        surname[20];
  int         age;
}rect_t;

rect_t inputRect (rect_t);
void   showRec   (rect_t res);
bool   saveRec   (rect_t res);
rect_t loadRec   (void);
#endif // _EX01_H_

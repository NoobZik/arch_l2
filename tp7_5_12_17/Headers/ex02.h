/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ex02.h                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: NoobZik <rakib.hernandez@gmail.com>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/12/05 15:03:46 by NoobZik           #+#    #+#             */
/*   Updated: 2017/12/05 15:43:52 by NoobZik          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ex01.h"
#ifndef _EX02_H_
#define _EX02_H_

#include <stdlib.h>

rect_t *createArray (size_t n);
void    deleteArray (rect_t *r);
bool    saveArray   (rect_t *r, size_t n);
rect_t *loadArray   (void);
void    shownArray  (rect_t *r);
#endif // _EX02_H_

/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ex02.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: NoobZik <rakib.hernandez@gmail.com>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/12/05 15:01:13 by NoobZik           #+#    #+#             */
/*   Updated: 2017/12/05 15:26:12 by NoobZik          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../Headers/ex02.h"
#include <string.h>
#include <stdbool.h>
#include <assert.h>
#include <unistd.h>
#include <fcntl.h>
#include <stdlib.h>
#include <stdio.h>


rect_t *createArray (size_t n) {
  rect_t *res = 0;
  res = malloc(sizeof(rect_t)*n);
  return res;
}

inline void deleteArray (rect_t *r) {
  free(r);
}

bool saveArray (rect_t *r, size_t n) {
  int fd;
  int i = -1;

  fd = open("array.txt", O_WRONLY | O_CREAT, 0666);
  if (fd == -1) perror("open");
  assert(fd != -1);

  while (++i < (int) n) {
    if(write(fd, &r[i], sizeof(rect_t)) == -1) {
      perror("write");
      return false;
    }
  }
  return (close(fd) != -1) ? true : false;
}

/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: NoobZik <rakib.hernandez@gmail.com>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/12/05 15:09:15 by NoobZik           #+#    #+#             */
/*   Updated: 2017/12/05 15:09:57 by NoobZik          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../Headers/ex01.h"
#include "../Headers/ex02.h"
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <fcntl.h>
#include <sys/types.h>
#include <assert.h>
#include <unistd.h>

/**
 * Main entry
 * @param argc [description]
 * @param argv [description]
 * @return [description]
 */
int main(int argc, char const *argv[]) {
  (void) argc;
  (void) argv;

  rect_t res;
  res = inputRect(res);

  showRec(res);

  (saveRec(res)) ?
    puts("Fichié sauvegardé") : puts("Erreur de sauvegarde");

  rect_t res1 = loadRec();
  showRec(res1);

  return 0;
}

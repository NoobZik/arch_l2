/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ex01.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: NoobZik <rakib.hernandez@gmail.com>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/12/05 10:55:29 by NoobZik           #+#    #+#             */
/*   Updated: 2017/12/05 15:09:05 by NoobZik          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../Headers/ex01.h"
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <fcntl.h>
#include <sys/types.h>
#include <assert.h>
#include <unistd.h>


/**
 * Ask the guy to fill out a structure
 * @param res [description]
 * @return [description]
 */
rect_t inputRect (rect_t res) {
  puts("Saissiez un nom : ");
  scanf("%s", res.name);
  puts("Saissiez un blaze : ");
  scanf("%s", res.surname);
  puts("Saissez un age : ");
  scanf("%d", &res.age);
  return res;
}

/**
 * Show the current structure content
 * @param res [description]
 */
void showRec (rect_t res) {
  puts("Vous avez saisi :");
  printf("Nom : %s\n", res.name);
  printf("Blaze : %s\n", res.surname);
  printf("Age : %d\n", res.age);
}

/**
 * Saves the content into a file name fout.txt
 * @param res [description]
 * @return [description]
 */
bool saveRec (rect_t res) {
  int fd;

  fd = open("fout.txt", O_WRONLY | O_CREAT, 0666);
  if (fd == -1)
    perror("Open");
  assert(fd != -1);
  return ((write(fd, &res, sizeof(rect_t)) != -1) && close(fd) != -1) ?
   true : false;
}

/**
 * Loads the content info the memeory
 * @return [description]
 */
rect_t loadRec () {
  int fd;
  rect_t res;
  puts("Chargement en cours...");
  fd = open("fout.txt", O_RDONLY, 0666);
  if (fd == -1)
    perror("Open :");
  assert(fd != -1);
  if(read(fd, &res, sizeof(rect_t))) {
    return res;
  }
  return res;
}

/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ex01.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: NoobZik <rakib.hernandez@gmail.com>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/11/21 10:14:47 by NoobZik           #+#    #+#             */
/*   Updated: 2017/11/21 11:10:10 by NoobZik          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

/* Ecrire une programme qui crée deux fils */

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>

int main(int argc, char const *argv[]) {
	(void) argc;
	(void) argv;

	pid_t pid;

	if ( (pid = fork()) == -1) {
		perror("Erreur fork :");
		exit(0);
	}
	if (pid > 0)
		printf("Pid is positive\n"
		       "Parent : %d\n"
					 "Child is %d\n", getppid(), getpid());

	if (pid == 0) {
		printf("pid is 0\n"
		       "Parent : %d\n"
		       "Child : %d\n", getppid(), getpid());
	}

	exit(0);

	return 0;
}

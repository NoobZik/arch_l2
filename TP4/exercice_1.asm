; **************************************************************************** ;
;                                                                              ;
;                                                         :::      ::::::::    ;
;    exercice_1.asm                                     :+:      :+:    :+:    ;
;                                                     +:+ +:+         +:+      ;
;    By: NoobZik <rakib.hernandez@gmail.com>        +#+  +:+       +#+         ;
;                                                 +#+#+#+#+#+   +#+            ;
;    Created: 2017/10/10 10:19:50 by NoobZik           #+#    #+#              ;
;    Updated: 2017/10/10 11:13:34 by NoobZik          ###   ########.fr        ;
;                                                                              ;
; **************************************************************************** ;

.ORIG x3000

main:	LEA 	R6, stackend
			LEA 	R1,		tab
			AND		R2,R2, 0
			ADD		R2,R2, 4
			JSR		Maximum
			TRAP X25 ; ou HALT

; Push R1
; Push R2
; Push R3
Maximum:	LDR R1, R6, 4
					LDR R3, R1, 0
					LDR R2, R6, 3
					BRZ sorti_proc
					LDR	R4,	R1, 0
					NOT R4,	R4
					ADD R4,	R4, 1
					AND,R5,	R5,	0
					ADD,R5,R3,R4
					BRm changer.max

tab:	.FILL 5
			.FILL 8
			.FILL 10
			.FILL 13
			.BKLW 100
stackend:
.END

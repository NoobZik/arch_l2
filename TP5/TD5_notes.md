# Architecture et sytseme TD5 (Preparation TP4 noté) #

### **Exercice 1** ###

1.  Pour initialiser des registres à 0, Il faut faire
```asm
AND R0, R0, R0
```


2.  Pour transferer des valeur entre deux registre il faut faire
```asm
ADD R3, R0, 0
```

3.  Avec LD et ST on peut gerer la copie des données entre deux mémoires.

LD = Memoire --> Registre
ST = Registre --> Memoire

4.  LEA Permet de récupérer l'adresse d'une mémoire et le stocker dans un régistre

```asm
LEA R2, m
```
### **Exercice 2** ###

**1.**

```asm
.ORIG x3000
LD R0, m
BRp pos
BR Fin

pos: NOT R0, R0
     ADD R0, R0
     ST  R0, res

RES: .BKLW 1 ; On connais pas, du coup on reserve un mot de mémoire
m: .FILL 50

Fin: .END
```

**2.**

```asm
.ORIG x3000
LD R0, m
BRp pos
BRn neg

BR Fin

pos: NOT R0, R0
     ADD R0, R0
     ST  R0, res
     BR Fin

neg: AND R1, R1, 0
     ADD R1, R1, -1
     ST R1, RES
     BR Fin

RES: .BKLW 1 ; On connais pas, du coup on reserve un mot de mémoire
m: .FILL 50

Fin: .END

```
### **Exercice 3** ###

| 2  | 3   | 4   |
|:-- |:----|:----|
| 6  |  7  | 8   |


```asm
.ORIG x3000

LEA R1, tab1
LEA R2, tab2
LEA R3, tab3

LD R0, taille
boucle:
	BRz fin

	LDR R4, R1, 0
	LDR R5, R2, 0

	ADD R4, R4, R5

	STR R4, R3, 0
	ADD R1, R1, 1
	ADD R2, R2, 1
	ADD R3, R3, 1
	ADD R0, R0, -1
	BR Boucle

taille : .FILL 3

tab2:
  .FILL 6
	.FILL 7
	.FILL 8

tabl1:
  .FILL 2
	.FILL 3
	.FILL 4

tab3: .BLKW 3

fin: .END
```

### **Exercice 4** ###
```asm

```

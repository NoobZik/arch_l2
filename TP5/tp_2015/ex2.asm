; **************************************************************************** ;
;                                                                              ;
;                                                         :::      ::::::::    ;
;    ex2.asm                                            :+:      :+:    :+:    ;
;                                                     +:+ +:+         +:+      ;
;    By: NoobZik <rakib.hernandez@gmail.com>        +#+  +:+       +#+         ;
;                                                 +#+#+#+#+#+   +#+            ;
;    Created: 2017/10/16 22:52:10 by NoobZik           #+#    #+#              ;
;    Updated: 2017/10/17 10:15:02 by NoobZik          ###   ########.fr        ;
;                                                                              ;
; **************************************************************************** ;

;1Rakib Sheikh / NoobZik
; 23/06/1997
; 11502605

; Remplissage d'un tableau de longueur n avec les premiers n nombres positifs
; multiples de 3

.ORIG x3000
	LD R0, longueur
	LEA R1, tableau
loop:	BRz end

			ADD R0, R0, -1

tableau: .BKLW longueur
longueur: ;Faut faire j1+j2+10
end: HALT
j1: .FILL 23
j2: .FILL 1

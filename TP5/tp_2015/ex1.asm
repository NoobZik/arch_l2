; **************************************************************************** ;
;                                                                              ;
;                                                         :::      ::::::::    ;
;    ex1.asm                                            :+:      :+:    :+:    ;
;                                                     +:+ +:+         +:+      ;
;    By: NoobZik <rakib.hernandez@gmail.com>        +#+  +:+       +#+         ;
;                                                 +#+#+#+#+#+   +#+            ;
;    Created: 2017/10/16 21:46:08 by NoobZik           #+#    #+#              ;
;    Updated: 2017/10/17 10:15:05 by NoobZik          ###   ########.fr        ;
;                                                                              ;
; **************************************************************************** ;

; Rakib Sheikh / NoobZik
; 23/06/1997
; 11502605

; Ecrire un programme assembleur LC-3 qui, étant données deux constantes A et B
; Calcule C = 3A / D = 4B E = C-y1 F = y+2 G = C+D-E(E+F)

; R3 = C
; R4 = D
; R5 = E
; R6 = F
; R7 = G

.ORIG x3000

JSR C
HALT


A : .FILL 2605 ; Constante numéro etudiant 1

B : .FILL 6093 ; Constante numéro etudiant 2

C : LD R0, A
		ADD R3, R0, R0		;R3 = A+A
		ADD R3, R3, R0 		;R3 = R3 + A


D : LD  R0, B
		ADD R4, R0, R0		;R4 = B + B
		ADD R4, R4, R4	;R4 = R4 + R4


E : LD  R0, y1			;R0 = y1
		NOT R0, R0			;R0 = -R0
		ADD R0, R0, 1		;R0+1 (completude)
		ADD R5, R3, R0	;R5 = R5 + R0

F : LD  R0, y2			;R0 = y2
		ADD R6, R3, R0	;R6 = R3 + R0 (C+y2)

G : ADD R7, R3, R4	;R7 = R3 + R4 (C + D)
		ADD R0, R5, R6	;R0 = R5 + R6 (E+F)
		NOT R0, R0			;Complétude de R0 vers -R0
		ADD R0, R0, 1		;FInalisation de la completude -R0+1
		ADD R7, R7, R0	;Opérateur principale R7 + R0
		HALT

y1: .FILL 1997
y2: .FILL 1998

.END

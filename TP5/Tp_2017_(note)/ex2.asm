; **************************************************************************** ;
;                                                                              ;
;                                                         :::      ::::::::    ;
;    ex2.asm                                            :+:      :+:    :+:    ;
;                                                     +:+ +:+         +:+      ;
;    By: NoobZik <rakib.hernandez@gmail.com>        +#+  +:+       +#+         ;
;                                                 +#+#+#+#+#+   +#+            ;
;    Created: 2017/10/17 10:30:10 by NoobZik           #+#    #+#              ;
;    Updated: 2017/10/17 11:15:47 by NoobZik          ###   ########.fr        ;
;                                                                              ;
; **************************************************************************** ;

; Rakib Sheikh / NoobZik
; 11502605
; 23/06/1997

; Samy Abdouche
; 11606892
; 01/04/1998

.ORIG x3000

	;; Init R3 à 0, chargement R0<-x et R1<-y

	AND R3, R3, 0
	LD R0, x
	LD R1, y

  ;; Negation pour faire -R1 par completude

	NOT R1, R1
	ADD R1, R1, 1

	;; X - y
	ADD R0, R0, R1

  ;; On regarde si la somme est n ou p ou z
	BRp pos
	BRz nulle
	BRn neg

	;; Securité de fin de prog au cas ou

	BR fin

	;; Si c'est position, z = 1
	;; SI c'est negatif, z = -1
	;; Si c'est egale, z = 0

pos:
	ADD R3, R3, 1
	ST R3, z
	BR fin

nulle:
	ST R3, z
	BR fin

neg:
	ADD R3, R3, -1
	ST  R3, z
	BR fin

x: .FILL 6
y: .FILL 7
z: .BLKW 1
fin: .END

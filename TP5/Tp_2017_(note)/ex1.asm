; **************************************************************************** ;
;                                                                              ;
;                                                         :::      ::::::::    ;
;    ex1.asm                                            :+:      :+:    :+:    ;
;                                                     +:+ +:+         +:+      ;
;    By: NoobZik <rakib.hernandez@gmail.com>        +#+  +:+       +#+         ;
;                                                 +#+#+#+#+#+   +#+            ;
;    Created: 2017/10/17 10:18:31 by NoobZik           #+#    #+#              ;
;    Updated: 2017/10/17 11:15:13 by NoobZik          ###   ########.fr        ;
;                                                                              ;
; **************************************************************************** ;

; Rakib Sheikh / NoobZik
; 11502605


; Samy Abdouche
; 11606892


.ORIG x3000

	AND R3,R3, 0 ; R3 init 0

	ADD R1, R2, 0; R1 = R2

	LD R4, x
	ST R4, y

	LEA R4, x
	ST R4, z
	BR fin

x: .FILL 5
y: .BLKW 1
z: .BLKW 1

fin: .END

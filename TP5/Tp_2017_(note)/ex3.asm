; **************************************************************************** ;
;                                                                              ;
;                                                         :::      ::::::::    ;
;    ex3.asm                                            :+:      :+:    :+:    ;
;                                                     +:+ +:+         +:+      ;
;    By: NoobZik <rakib.hernandez@gmail.com>        +#+  +:+       +#+         ;
;                                                 +#+#+#+#+#+   +#+            ;
;    Created: 2017/10/17 10:46:17 by NoobZik           #+#    #+#              ;
;    Updated: 2017/10/17 11:06:00 by NoobZik          ###   ########.fr        ;
;                                                                              ;
; **************************************************************************** ;

; Rakib Sheikh / NoobZik
; 11502605
; 23/06/1997

; Samy Abdouche
; 11606892
; 01/04/1998

.ORIG x3000
	LEA R1, tab1
	LEA R2, tab2

	LD R0, taille

boucle:
	;; On teste si la taille du tableau stocké dans R0 est nulle
  BRz fin

	LDR R4, R1, 0

	STR R4, R2, 0

	;; On se décale d'une case tab1 et tab 2

	ADD R1, R1, 1
	ADD R2, R2, 1

	;; On décrémente la taille du tableau stocké dans R0

	ADD R0, R0, -1
	BR boucle

taille: .FILL 3

tab1:
	.FILL 3
	.FILL 4
	.FILL 5

tab2: .BLKW 3

fin:
	.END

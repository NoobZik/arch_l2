; **************************************************************************** ;
;                                                                              ;
;                                                         :::      ::::::::    ;
;    exercice1.asm                                      :+:      :+:    :+:    ;
;                                                     +:+ +:+         +:+      ;
;    By: NoobZik <rakib.hernandez@gmail.com>        +#+  +:+       +#+         ;
;                                                 +#+#+#+#+#+   +#+            ;
;    Created: 2017/10/03 10:35:31 by NoobZik           #+#    #+#              ;
;    Updated: 2017/10/03 15:46:52 by NoobZik          ###   ########.fr        ;
;                                                                              ;
; **************************************************************************** ;

.ORIG x3000

LD R0, C1		; RO := C1
LD R1, C3		; R1 := C3
; Complétude de C3 (-C3)
; On doit inverser avec not et rajouter +1
NOT R1, R1
ADD R1, R1, 1		  ; R1 := -C3
ADD R0, R0, R1		; R0 := C1 - C3
LD R1, C2		      ; R1 := C2
ADD R1, R1, 9		  ; R1 := C2 + 9
ADD R1, R1, R1		; R1 := 2(C2+9)
ADD R0, R0, R1		; R0 := (C1-C3)+2(C2+9)
LD R1, K		      ; R1 := -128
ADD R0, R0, R1		; resultat
ST R0, RES
HALT

K:	.FILL -128
C1:	.FILL 10
C2:	.FILL -4
C3:	.FILL 6
RES:	.BLKW 1
.END

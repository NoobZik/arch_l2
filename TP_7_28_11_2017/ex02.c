/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ex02.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: NoobZik <rakib.hernandez@gmail.com>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/11/28 11:03:53 by NoobZik           #+#    #+#             */
/*   Updated: 2017/11/28 11:30:56 by NoobZik          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>

/**
 * [main description]
 * @param argc [description]
 * @param argv [description]
 * @return [description]
 */

int main(int argc, char const *argv[]) {
	(void) argc;
	(void) argv;

	int x;
	pid_t pidfils = fork();

	x = 0;

	(pidfils == 0) ? x = 1 : printf("x = %d\n", x);

	return 0;
}

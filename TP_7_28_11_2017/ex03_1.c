/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ex03.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: NoobZik <rakib.hernandez@gmail.com>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/11/28 11:32:27 by NoobZik           #+#    #+#             */
/*   Updated: 2017/11/28 11:35:49 by NoobZik          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <sys/types.h>

/**
 * Showing 2 times
 * @param argc [description]
 * @param argv [description]
 * @return [description]
 */
int main(int argc, char const *argv[]) {
	(void) argc;
	(void) argv;

	fork();
	printf("Hello\n");
	exit(0);
	return 0;
}

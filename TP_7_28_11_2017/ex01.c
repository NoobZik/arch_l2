/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ex01.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: NoobZik <rakib.hernandez@gmail.com>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/11/28 10:20:53 by NoobZik           #+#    #+#             */
/*   Updated: 2017/11/28 11:30:47 by NoobZik          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int main(int argc, char const *argv[]) {
	int i = 0;
	if (argc < 2) {
		printf("FDP\n");
		return 0;
	}

	char test[100] = "";

	while (++i < argc) {
		strcat(test, argv[i]);
		strcat(test, " ");
	}

	system(test);
	return 0;
}
